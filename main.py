import psycopg2


def run_select(select: str, host: str, port: int, user: str, password: str, dbname=str):
    """

    :param select:
        select
    :param host: host db
    :param port: port db
    :param user: user db
    :param password: password db
    :return: pd.DataFrame
    """
    data_return = []
    try:
        conn = psycopg2.connect(host=host, port=port, user=user, password=password, dbname=dbname)
        cur = conn.cursor()
        cur.execute(select)
        obj = cur.fetchall()
        columns = [item.name for item in cur.description]
        data_return = [columns, obj]


    except (Exception, psycopg2.Error) as error:
        print("Error while fetching data from PostgreSQL", error)

    finally:
        # closing database connection.
        if conn:
            cur.close()
            conn.close()
            print("PostgreSQL connection is closed")

    return data_return


def run_update(update: str, host: str, port: int, user: str, password: str, dbname=str):
    """

    :param select:
        select
    :param host: host db
    :param port: port db
    :param user: user db
    :param password: password db
    :return: pd.DataFrame
    """
    data_return = []
    try:
        conn = psycopg2.connect(host=host, port=port, user=user, password=password, dbname=dbname)
        cur = conn.cursor()
        cur.execute(update)
        conn.commit()

    except (Exception, psycopg2.Error) as error:
        print("Error while fetching data from PostgreSQL", error)

    finally:
        # closing database connection.
        if conn:
            cur.close()
            conn.close()
            print("PostgreSQL connection is closed")

    return data_return


def matheus(foi_derrubado: bool = True):
    """
    Esta é funcao da vida do Matheus
    :param foi_derrubado:bool
        Variável que determina se alguem te derrubou
    :return: power
        Poder que voce ira se levantar
    """
    acesso = {"host": "mundo", "port": 0, "user": "qualquer_um",
     "password": "forabolsonaro", "dbname": "pessoas"}
    where = """
     name='Matheus Correia Politano' and
      city_born ='Catanduva' and
      city_that_grew = 'Agulha' and 
      birthday = '2000-12-18'"""
    select = """
    select power from my_life  where {where}
     """.format(where=where)
    data_select = run_select(select, **acesso)
    power = data_select[0][0]
    if foi_derrubado:
        power += 1
    update = "update my_life set power={power} where {where}".format(power=power, where=where)
    run_update(update, **acesso)

    return power











# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    matheus()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
